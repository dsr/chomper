/* global Phaser */
'use strict';
var Chomper = require('../prefabs/chomper');
var Ground = require('../prefabs/ground');
var Cookie = require('../prefabs/cookie');
// var cookiesOnScreen = 0;

function Play() {}
Play.prototype = {
    create: function () {
        this.maxCookies = 5;
        this.score = 0;
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.physics.arcade.gravity.y = 500;
        this.chomper = new Chomper(this.game, 100, this.game.height / 2);
        this.chomper.body.collideWorldBounds = true;
        this.ground = new Ground(this.game, 0, 500, 800, 100);
        this.game.add.existing(this.ground);
        this.chomper.events.onInputDown.add(this.clickListener, this);
        this.game.add.existing(this.chomper);
        this.cookieGroup = this.game.add.group();
        this.cookieGroup.enableBodies = true;
    },

    update: function () {
        // Handle collisions between chomper and the ground
        this.game.physics.arcade.collide(this.chomper, this.ground, this.groundCollision, null, this);
        this.game.physics.arcade.collide(this.chomper, this.cookieGroup, this.ateCookie, null, this);

        // Handle the pressing of direction keys
        var upKey = this.game.input.keyboard.addKey(Phaser.Keyboard.UP);
        var rightKey = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        var leftKey = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);

        // Assign chomper.jump to when the up key is pressed
        upKey.onDown.add(this.chomper.jump, this.chomper);
        // Mouse press on the screen makes chomper jump too
        this.input.onDown.add(this.chomper.jump, this.chomper);

		// Left and right movement
        if (rightKey.isDown) {
            this.chomper.moveRight();
        } else if (leftKey.isDown) {
            this.chomper.moveLeft();
        }
        // Add a cookie if there are less than 5 on screen
        if (this.cookieGroup.length < this.maxCookies) {
            this.makeCookies();
        }
    },
    // Check for ground collision so we can reset the jumping
    groundCollision: function () {
        this.chomper.jumping = false;
    },
    // This is here to test gameover state
    clickListener: function () {
        this.game.state.start('gameover');
    },
    // Make cookies
    makeCookies: function () {
        var cookie = new Cookie(this.game, this.game.rnd.between(600, 640), this.game.rnd.between(100, 480)); 
        cookie.events.onOutOfBounds.add(this.resetCookie, this);
        this.cookieGroup.add(cookie);
    },
    // cookie from group when necessary
    resetCookie: function (cookie) {
        cookie.reset(this.game.rnd.between(600, 640), this.game.rnd.between(100, 480));
        cookie.body.velocity.x = -200;
    },
    // Reset cookie when Chomper runs into it
    ateCookie: function (player, cookie) {
      this.resetCookie(cookie);
      this.score++;
      console.log(this.score);
    }
};

module.exports = Play;
