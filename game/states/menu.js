
'use strict';
var Chomper = require('../prefabs/chomper');
// var Ground = require('../prefabs/ground');
function Menu() {}

Menu.prototype = {
  preload: function () {

  },
  create: function () {
    var style = { font: '65px Arial', fill: '#ffffff', align: 'center'};
    this.chomper = new Chomper(this.game, (this.game.width / 2) - 32, 400);
    // this.chomper.body.velocity.x = 30;
    this.game.add.existing(this.chomper);
    this.titleText = this.game.add.text(this.game.world.centerX, 200, 'Chomper!', style);
    this.titleText.anchor.setTo(0.5, 0.5);

    this.instructionsText = this.game.add.text(
      this.game.world.centerX, 250, 'Click anywhere to play.',
      { font: '16px Arial', fill: '#ffffff', align: 'center'});
    this.instructionsText.anchor.setTo(0.5, 0.5);
    this.ground = this.game.add.tileSprite(0, 500, 800, 100, 'ground');
    this.ground.autoScroll(-200, 0);

   // this.sprite.angle = -20;
   // this.game.add.tween(this.sprite).to({angle: 20}, 1000, Phaser.Easing.Linear.NONE, true, 0, 1000, true);
  },
  update: function () {
    if (this.game.input.activePointer.justPressed()) {
      this.game.state.start('play');
    }
  }
};

module.exports = Menu;
