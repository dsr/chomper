/* global Phaser */
'use strict';

var Cookie = function (game, x, y, frame) {
    Phaser.Sprite.call(this, game, x, y, 'cookie', frame);
    this.game.physics.arcade.enableBody(this);
    this.body.velocity.x = -200;
    this.checkWorldBounds = true;
    this.killOutOfBoundsKill = true;
    this.body.allowGravity = false;

    };

Cookie.prototype = Object.create(Phaser.Sprite.prototype);
Cookie.prototype.constructor = Cookie;

Cookie.prototype.update = function () {

  // write your prefab's specific update code here

};

module.exports = Cookie;
