/* global Phaser */
'use strict';

var Chomper = function (game, x, y, frame) {
    Phaser.Sprite.call(this, game, x, y, 'little_chomper', frame);
    this.animations.add('chomp');
    this.play('chomp', 12, true);
    this.inputEnabled = true;
    this.game.physics.arcade.enableBody(this);
    this.jumping = false;
  };

Chomper.prototype = Object.create(Phaser.Sprite.prototype);
Chomper.prototype.constructor = Chomper;

Chomper.prototype.update = function () {
    Chomper.prototype.jump = function () {
        if (this.jumping === false) {
            this.body.velocity.y = -350;
            this.jumping = true;
        }
    };
    this.moveRight = function () {
        this.x += 5;
    };
    this.moveLeft = function () {
        this.x -= 5;
    };
};

module.exports = Chomper;
