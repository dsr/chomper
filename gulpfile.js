'use strict';

var gulp = require('gulp');
var connect = require('gulp-connect');
var browserify = require('browserify');
var rename = require('gulp-rename');
var source = require('vinyl-source-stream');
var hint = require('gulp-jshint');

gulp.task('default', function(){
	console.log('Ran this');
});

gulp.task('lint', function(){
	return gulp.src(['game/prefabs/*.js', 'game/states/*.js', '*.js'])
		.pipe(hint())
		.pipe(hint.reporter('default'));
});

gulp.task('connectDist', function(){
	connect.server({
		root: 'dist',
	});
});

gulp.task('browserify', function(){
	var bundleStream = browserify('./game/main.js').bundle();

	bundleStream
		.pipe(source('test.js'))
		.pipe(rename('game.js'))
		.pipe(gulp.dest('./dist/js/'));
});

gulp.task('watch', function  () {
	gulp.watch(['./dist/*.html', './game/*.js', './game/prefabs/*.js', './game/states/*.js', '*.js'], ['lint', 'browserify']);
});

gulp.task('default', ['connectDist', 'browserify', 'watch', 'lint']);
