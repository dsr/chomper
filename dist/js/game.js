(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

//global variables
window.onload = function () {
  var game = new Phaser.Game(800, 600, Phaser.AUTO, 'monsters')

  // Game States
  game.state.add('boot', require('./states/boot'));
  game.state.add('gameover', require('./states/gameover'));
  game.state.add('menu', require('./states/menu'));
  game.state.add('play', require('./states/play'));
  game.state.add('preload', require('./states/preload'));
  
  game.state.start('boot');
};
},{"./states/boot":5,"./states/gameover":6,"./states/menu":7,"./states/play":8,"./states/preload":9}],2:[function(require,module,exports){
/* global Phaser */
'use strict';

var Chomper = function (game, x, y, frame) {
    Phaser.Sprite.call(this, game, x, y, 'little_chomper', frame);
    this.animations.add('chomp');
    this.play('chomp', 12, true);
    this.inputEnabled = true;
    this.game.physics.arcade.enableBody(this);
    this.jumping = false;
  };

Chomper.prototype = Object.create(Phaser.Sprite.prototype);
Chomper.prototype.constructor = Chomper;

Chomper.prototype.update = function () {
    Chomper.prototype.jump = function () {
        if (this.jumping === false) {
            this.body.velocity.y = -350;
            this.jumping = true;
        }
    };
    this.moveRight = function () {
        this.x += 5;
    };
    this.moveLeft = function () {
        this.x -= 5;
    };
};

module.exports = Chomper;

},{}],3:[function(require,module,exports){
/* global Phaser */
'use strict';

var Cookie = function (game, x, y, frame) {
    Phaser.Sprite.call(this, game, x, y, 'cookie', frame);
    this.game.physics.arcade.enableBody(this);
    this.body.velocity.x = -200;
    this.checkWorldBounds = true;
    this.killOutOfBoundsKill = true;
    this.body.allowGravity = false;

    };

Cookie.prototype = Object.create(Phaser.Sprite.prototype);
Cookie.prototype.constructor = Cookie;

Cookie.prototype.update = function () {

  // write your prefab's specific update code here

};

module.exports = Cookie;

},{}],4:[function(require,module,exports){
/* global Phaser */
'use strict';

var Ground = function (game, x, y, width, height) {
  Phaser.TileSprite.call(this, game, x, y, width, height, 'ground');
  this.autoScroll(-200, 0);
  this.game.physics.arcade.enableBody(this);
  this.body.allowGravity = false;
  this.body.immovable = true;
};

Ground.prototype = Object.create(Phaser.TileSprite.prototype);
Ground.prototype.constructor = Ground;

Ground.prototype.update = function () {

  // write your prefab's specific update code here

};

module.exports = Ground;

},{}],5:[function(require,module,exports){

'use strict';

function Boot() {
}

Boot.prototype = {
  preload: function() {
    this.load.image('preloader', 'assets/preloader.gif');
  },
  create: function() {
    this.game.input.maxPointers = 1;
    this.game.state.start('preload');
  }
};

module.exports = Boot;

},{}],6:[function(require,module,exports){

'use strict';
function GameOver() {}

GameOver.prototype = {
  preload: function () {

  },
  create: function () {
    var style = { font: '65px Arial', fill: '#ffffff', align: 'center'};
    this.titleText = this.game.add.text(this.game.world.centerX,100, 'Game Over!', style);
    this.titleText.anchor.setTo(0.5, 0.5);

    this.congratsText = this.game.add.text(this.game.world.centerX, 200, 'You Win!', { font: '32px Arial', fill: '#ffffff', align: 'center'});
    this.congratsText.anchor.setTo(0.5, 0.5);

    this.instructionText = this.game.add.text(this.game.world.centerX, 300, 'Click To Play Again', { font: '16px Arial', fill: '#ffffff', align: 'center'});
    this.instructionText.anchor.setTo(0.5, 0.5);
  },
  update: function () {
    if(this.game.input.activePointer.justPressed()) {
      this.game.state.start('play');
    }
  }
};
module.exports = GameOver;

},{}],7:[function(require,module,exports){

'use strict';
var Chomper = require('../prefabs/chomper');
// var Ground = require('../prefabs/ground');
function Menu() {}

Menu.prototype = {
  preload: function () {

  },
  create: function () {
    var style = { font: '65px Arial', fill: '#ffffff', align: 'center'};
    this.chomper = new Chomper(this.game, (this.game.width / 2) - 32, 400);
    // this.chomper.body.velocity.x = 30;
    this.game.add.existing(this.chomper);
    this.titleText = this.game.add.text(this.game.world.centerX, 200, 'Chomper!', style);
    this.titleText.anchor.setTo(0.5, 0.5);

    this.instructionsText = this.game.add.text(
      this.game.world.centerX, 250, 'Click anywhere to play.',
      { font: '16px Arial', fill: '#ffffff', align: 'center'});
    this.instructionsText.anchor.setTo(0.5, 0.5);
    this.ground = this.game.add.tileSprite(0, 500, 800, 100, 'ground');
    this.ground.autoScroll(-200, 0);

   // this.sprite.angle = -20;
   // this.game.add.tween(this.sprite).to({angle: 20}, 1000, Phaser.Easing.Linear.NONE, true, 0, 1000, true);
  },
  update: function () {
    if (this.game.input.activePointer.justPressed()) {
      this.game.state.start('play');
    }
  }
};

module.exports = Menu;

},{"../prefabs/chomper":2}],8:[function(require,module,exports){
/* global Phaser */
'use strict';
var Chomper = require('../prefabs/chomper');
var Ground = require('../prefabs/ground');
var Cookie = require('../prefabs/cookie');
// var cookiesOnScreen = 0;

function Play() {}
Play.prototype = {
    create: function () {
        this.maxCookies = 5;
        this.score = 0;
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.physics.arcade.gravity.y = 500;
        this.chomper = new Chomper(this.game, 100, this.game.height / 2);
        this.chomper.body.collideWorldBounds = true;
        this.ground = new Ground(this.game, 0, 500, 800, 100);
        this.game.add.existing(this.ground);
        this.chomper.events.onInputDown.add(this.clickListener, this);
        this.game.add.existing(this.chomper);
        this.cookieGroup = this.game.add.group();
        this.cookieGroup.enableBodies = true;
    },

    update: function () {
        // Handle collisions between chomper and the ground
        this.game.physics.arcade.collide(this.chomper, this.ground, this.groundCollision, null, this);
        this.game.physics.arcade.collide(this.chomper, this.cookieGroup, this.ateCookie, null, this);

        // Handle the pressing of direction keys
        var upKey = this.game.input.keyboard.addKey(Phaser.Keyboard.UP);
        var rightKey = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        var leftKey = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);

        // Assign chomper.jump to when the up key is pressed
        upKey.onDown.add(this.chomper.jump, this.chomper);
        // Mouse press on the screen makes chomper jump too
        this.input.onDown.add(this.chomper.jump, this.chomper);

		// Left and right movement
        if (rightKey.isDown) {
            this.chomper.moveRight();
        } else if (leftKey.isDown) {
            this.chomper.moveLeft();
        }
        // Add a cookie if there are less than 5 on screen
        if (this.cookieGroup.length < this.maxCookies) {
            this.makeCookies();
        }
    },
    // Check for ground collision so we can reset the jumping
    groundCollision: function () {
        this.chomper.jumping = false;
    },
    // This is here to test gameover state
    clickListener: function () {
        this.game.state.start('gameover');
    },
    // Make cookies
    makeCookies: function () {
        var cookie = new Cookie(this.game, this.game.rnd.between(600, 640), this.game.rnd.between(100, 480)); 
        cookie.events.onOutOfBounds.add(this.resetCookie, this);
        this.cookieGroup.add(cookie);
    },
    // cookie from group when necessary
    resetCookie: function (cookie) {
        cookie.reset(this.game.rnd.between(600, 640), this.game.rnd.between(100, 480));
        cookie.body.velocity.x = -200;
    },
    // Reset cookie when Chomper runs into it
    ateCookie: function (player, cookie) {
      this.resetCookie(cookie);
      this.score++;
      console.log(this.score);
    }
};

module.exports = Play;

},{"../prefabs/chomper":2,"../prefabs/cookie":3,"../prefabs/ground":4}],9:[function(require,module,exports){

'use strict';
function Preload() {
  this.asset = null;
  this.ready = false;
}

Preload.prototype = {
  preload: function () {
    this.asset = this.add.sprite(this.width / 2, this.height / 2, 'preloader');
    this.asset.anchor.setTo(0.5, 0.5);

    this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
    this.load.setPreloadSprite(this.asset);
    // this.load.image('bluebox', 'assets/bluebox.png');

    this.load.spritesheet('little_chomper', 'assets/bluebox_small.png', 64, 64, 3);
    this.load.image('ground', 'assets/ground2.png');
    this.load.image('cookie', 'assets/cookie.png');


  },
  create: function () {
    this.asset.cropEnabled = false;
  },
  update: function () {
    if (!!this.ready) {
      this.game.state.start('menu');
    }
  },
  onLoadComplete: function () {
    this.ready = true;
  }
};

module.exports = Preload;

},{}]},{},[1])